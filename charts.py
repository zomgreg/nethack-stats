import matplotlib.pyplot as plt

def pie_chart(labels, values):
    plt.pie(values, labels=labels, shadow=True, startangle=90)

    plt.legend([values, labels], loc='upper left')

    plt.show()
